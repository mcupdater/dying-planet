#!/bin/bash

TMP=`mktemp -d --tmpdir=.`
mkdir -v "${TMP}"
cp -rf mods config "${TMP}"

REV=`date +%Y.%m.%d`
NUM=`ls fastpack-${REV}.* | wc -l`
REV="${REV}.${NUM}"

java -jar MCU-FastPack-latest.jar --mc 1.6.4 --forge 9.11.1.965  --path "${TMP}" --baseURL http://files.mcupdater.com/dying_planet --out fastpack-${REV}.xml --id dying_planet --name "Dying Planet" --revision "${REV}"

rm -rf "${TMP}"
